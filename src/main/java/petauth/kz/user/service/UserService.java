package petauth.kz.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import petauth.kz.user.dao.UserRepo;
import petauth.kz.user.dto.User;

import java.util.List;

/**
 * Service class for {@link petauth.kz.user.dto.User}
 *
 * @author Ilyas Burkhanov
 * */
@Service
public class UserService {

    @Autowired
    UserRepo  repo;


    public List<User> getAllUsers(){

        return repo.findAll();
    }

    public User register(User user) {
        return repo.saveAndFlush(user);
    }

    public void delete(User user){

         repo.delete(user);
    }

    public User getUserBase64(String username,String password){
        return  repo.getUserForBase64(username,password);
    }

    public User getCurrentUser(String username){
        return  repo.getCurrentUser(username);
    }


    public List<User> search(String key) {
        return repo.search(key);
    }


    public int editFullname(String newName) {
        return repo.editFullname(newName, getCurrentUsername());
    }
    private String getCurrentUsername() { //бітпеген. бітір
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        String username=authentication.getName();
        System.out.println("getCurrentUser is : " +username);
        return username;



    }


    public int editPassword(String newPassword, String oldPassword) {
        System.out.println("newPassword : " +newPassword+ " and oldPassword : " +oldPassword);
       System.out.println("Current user password "+getCurrentUser(getCurrentUsername()).getPassword());
        if (getCurrentUser(getCurrentUsername()).getPassword().contains(oldPassword)){
            return  repo.editPassword(newPassword, getCurrentUsername());
        }else {
            return 0;
        }

    }
    public int editAvatar(String avatar) {
        System.out.println("newAvatar : " +avatar);
        return  repo.editAvatar(avatar, getCurrentUsername());


    }
    public int editToken(String token) {
        System.out.println("newAvatar : " +token);
        return  repo.editToken(token, getCurrentUsername());


    }
}
