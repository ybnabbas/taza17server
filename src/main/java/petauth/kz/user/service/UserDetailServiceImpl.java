package petauth.kz.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import petauth.kz.user.dao.UserRepo;
import petauth.kz.user.dto.User;

/**
 * Implementation {@link UserDetailsService}
 *
 * @author Ilyas Burkhanov
 */


public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("username ...." +username);

        User user = userRepo.findByUsername(username);
        System.out.println("loadUserByUsername...." +user.getPassword());
        UserBuilder userBuilder = null;
        if (user != null) {
            System.out.println("User here : " + user.getFullName());
            userBuilder = org.springframework.security.core.userdetails.User.withUsername(username);

            userBuilder.roles(user.getRole().getName());
            userBuilder.password(bCryptPasswordEncoder().encode(user.getPassword()));

        } else throw new UsernameNotFoundException("User not found");
        return userBuilder.build();
    }

}
