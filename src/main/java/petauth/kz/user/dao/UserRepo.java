package petauth.kz.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import petauth.kz.user.dto.User;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link User}
 *
 * */
@Repository
public interface UserRepo extends JpaRepository<User,String> {


    User findByUsername(String username);

    @Query("select u from User u where u.enabled=:enabled ")
    List<User> findAllByEnabled(@Param("enabled") boolean enabled);

    @Query("select u from User u ")
    List<User> findAll();

    @Query("select u from User u where u.username=:username and u.password=:password")
    User getUserForBase64(@Param("username") String username, @Param("password") String password);

    @Query("select u from User u where u.username=:username")
    User getCurrentUser(@Param("username") String username);

    @Query(value = "select * from users where id=:id",nativeQuery = true)
    User getUserById(@Param("id") Integer id);

    @Query("select u from User u where u.username like %:key% or u.fullName like %:key%")
    List<User> search(@Param("key") String key);
    @Transactional
    @Modifying
    @Query("update User u set u.fullName=:newName where u.username=:login")
    int editFullname(@Param("newName")String newName, @Param("login") String login);

    @Transactional
    @Modifying
    @Query("update User u set u.password=:newPassword where u.username=:login")
    int editPassword(@Param("newPassword")String newPassword,@Param("login") String login);

    @Transactional
    @Modifying
    @Query("update User u set u.avatar=:avatar where u.username=:login")
    int editAvatar(@Param("avatar")String avatar,@Param("login") String login);

    @Transactional
    @Modifying
    @Query("update User u set u.token=:token where u.username=:login")
    int editToken(@Param("token")String token,@Param("login") String login);


}
