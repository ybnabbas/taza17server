package petauth.kz.user.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import petauth.kz.user.dto.User;
import petauth.kz.user.service.UserService;

@RestController
@CrossOrigin
@RequestMapping("/editprofile")
public class EditProfileController {
@Autowired
    UserService service;
    @PostMapping(value = "/fullname", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity fullname(@RequestBody String newName){
        System.out.println("newName is :" +newName);
        JSONObject jsonObject = new JSONObject(newName);

        if (service.editFullname(jsonObject.getString("newName"))==1){
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }


    }
    @PostMapping(value = "/password", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity password(@RequestBody String newPassword ){
        JSONObject jsonObject = new JSONObject(newPassword);


        if (service.editPassword(jsonObject.getString("newPassword"),jsonObject.getString("oldPassword"))==1){
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }


    }
    @PostMapping(value = "/avatar", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity avatar(@RequestBody String avatar ){
        System.out.println("avatar is : " +avatar);
       // JSONObject jsonObject = new JSONObject(avatar);


        if (service.editAvatar(avatar)==1){
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }


    }
    @PostMapping(value = "/token", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity token(@RequestBody String token ){
        System.out.println("token is : " +token);
        // JSONObject jsonObject = new JSONObject(avatar);
        if (service.editToken(token)==1){
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }


    }


    @GetMapping
    public String tester(){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        String username=authentication.getName();
        System.out.println("getCurrentUser is : " +username);
        return username;

    }


}
