package petauth.kz.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import petauth.kz.FCM;
import petauth.kz.user.dto.Role;
import petauth.kz.user.dto.User;
import petauth.kz.user.service.UserService;
import utils.ResponseMessage;

import java.util.Base64;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService service;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity registerBase64(@RequestBody User user) {
        Role role = new Role();
        role.setId(3);  // 3 ол USER ;
        user.setRole(role);
        System.out.println("Before Registered  user : " +user.toString());

        try {
            User result=service.register(user);
            if (result != null) {
                String forBase = user.getUsername() + ":" + user.getPassword();
                System.out.println("After Registered  user : " + result.toString());

                return new ResponseEntity(new ResponseMessage(Base64.getEncoder().encodeToString(forBase.getBytes())), HttpStatus.OK);


            } else {
                return new ResponseEntity(new ResponseMessage("Серверде қателік"), HttpStatus.NOT_FOUND);
            }
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            System.out.println("Already exist");
              return new ResponseEntity(new ResponseMessage(user.getUsername() +" already exist"), HttpStatus.CONFLICT);
        }


    }
@Autowired
    FCM fcm;

    @GetMapping("/all")
    public List<User> getAllUsers(){

        return service.getAllUsers();

    }
    @GetMapping(value = "/authorize")
    ResponseEntity authorizeBase64(@RequestParam("username") String username,@RequestParam("password") String password){
        User result=service.getUserBase64(username,password);
        if (result==null){
            return new ResponseEntity(new ResponseMessage("Телефон немесе құпия сөз дұрыс емес. Қайталаңыз"),HttpStatus.NOT_FOUND);
        }else {
              String forBase = result.getUsername() + ":" + result.getPassword();
            return new ResponseEntity(new ResponseMessage(Base64.getEncoder().encodeToString(forBase.getBytes())),HttpStatus.OK);
        }

    }

    @GetMapping("/search")
    public List<User> searchUser(@RequestParam("key") String key){
        return service.search(key);
    }

    @GetMapping
    public User getCurrent() {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        String username=authentication.getName();
        System.out.println("getCurrentUser is : " +username);
        return service.getCurrentUser(username);



    }

}
