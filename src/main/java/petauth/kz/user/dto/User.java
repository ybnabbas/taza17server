package petauth.kz.user.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import petauth.kz.message.dto.Message;
import petauth.kz.taza.dto.Taza;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User  {
    @Id
    @Column(name = "id",columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(name = "fullname")
    String fullName;

    @Column(name = "username",unique = true)
    String username;

    @Column(name = "password")
    String password;

    @Column(name = "score")
    int score;

    @Column(name = "enabled")
    boolean enabled=true;

    @JsonManagedReference
    @OneToMany(mappedBy = "tazaOwner")
    List<Taza> tazaCollection ;

    @JsonManagedReference
    @OneToMany(mappedBy = "messageOwner")
    List<Message> messageCollection;


    @ManyToOne
    @JoinColumn(name = "role")
    Role role;

    @Column(name = "avatar",columnDefinition = "text")
    String avatar;
    @Column(name = "token",columnDefinition = "text")
    String token;

//    @OneToMany(mappedBy = "mapOwner")
//    @JsonBackReference
//    Collection<Map> collection1 ;



}
