package petauth.kz.shop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import petauth.kz.shop.dto.Product;
import petauth.kz.shop.service.ProductService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/product")

public class ProductController {
    @Autowired
    ProductService service;


    @GetMapping
    List<Product> products(){
        return service.products();
    }
    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity save(@RequestBody Product product){
        System.out.println(product.toString());
        if (service.save(product)){
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }
    }
}
