package petauth.kz.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import petauth.kz.shop.dao.ProductRepo;
import petauth.kz.shop.dto.Product;

import java.util.List;

@Service
public class ProductService {
@Autowired
    ProductRepo repo;
    public List<Product> products() {
        return repo.findAll();
    }

    public boolean save(Product product) {
        Product result=repo.saveAndFlush(product);
        if (result!=null){
            return true;
        }else {
            return false;
        }
    }
}
