package petauth.kz.shop.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petauth.kz.shop.dto.Product;
@Repository
public interface ProductRepo extends JpaRepository<Product,Integer> {
}
