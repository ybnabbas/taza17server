package petauth.kz.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import petauth.kz.map.dto.Map;
import petauth.kz.map.service.MapService;
import petauth.kz.user.dto.Role;
import petauth.kz.user.dto.User;
import petauth.kz.user.service.UserService;

@RestController
@RequestMapping("/admin")
@PreAuthorize("hasRole('ADMIN')")
@CrossOrigin
public class AdminController {

    @Autowired
    UserService userService;
    @Autowired
    MapService mapService;

    @PostMapping(value = "/addmoderator",produces = MediaType.APPLICATION_JSON_UTF8_VALUE,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity addModerator(@RequestBody User user){

        System.out.println("addModerator :" +user.toString());
        Role role = new Role();
        role.setId(2); // 2 ол MODERATOR
        user.setRole(role);
        User result =userService.register(user);
        if (result!=null){
            Map map= new Map();
            map.setMapOwner(result);
            map.setLng("69.596329");
            map.setLat("42.340782");
            map.setEnabled(true);
            mapService.save(map);
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.badRequest().build();
        }

    }
    @PostMapping("/deletemoderator")
    ResponseEntity deleteModerator(@RequestBody User user) {

        userService.delete(user);
        return ResponseEntity.ok().build();
    }

}
