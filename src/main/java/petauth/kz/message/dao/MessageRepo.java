package petauth.kz.message.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import petauth.kz.message.dto.Message;

import petauth.kz.user.dto.User;

import java.util.List;

@Repository
public interface MessageRepo extends JpaRepository<Message,Long>{


    @Query("select message from Message message where message.messageOwner=:owner")
    List<Message> getAllByOwner(@Param("owner") User owner);


    @Query(value = "SELECT message FROM Message message ")
    List<Message> getAll();
}
