package petauth.kz.message.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import petauth.kz.FCM;
import petauth.kz.message.dao.MessageRepo;
import petauth.kz.message.dto.Message;
import petauth.kz.taza.dao.TazaRepo;
import petauth.kz.taza.dto.Taza;
import petauth.kz.taza.dto.TazaType;
import petauth.kz.user.dao.UserRepo;
import petauth.kz.user.dto.User;

import java.util.List;

/**
 * Service class from {@link Taza}
 */
@Service
public class MessageService {

    @Autowired
    MessageRepo repo;

    @Autowired
    FCM fcm;
    @Autowired
    UserRepo userRepo;


    public List<Message> getAllByOwner(User owner) {
        return repo.getAllByOwner(owner);
    }

//бұны көбінесе модератор шақырады
    public boolean save(Message message) {

        Message result = repo.saveAndFlush(message);
//        userRepo.findById(message.getMessageOwner().getId()+"");
        System.out.println("Saved message object is : " + result.getMessageOwner().getId());

        System.out.println("MessageOwner  is: " + userRepo.getUserById(message.getMessageOwner().getId()));


        JSONObject mainObject = new JSONObject();
        mainObject.put("title", message.getSubject());
        mainObject.put("body", message.getDescription());
        mainObject.put("sound", "default");

        JSONObject dataObject = new JSONObject();
        dataObject.put("type",2);


        JSONObject full = new JSONObject();
        System.out.println("to : "+message.getMessageOwner().getToken());
        full.put("to", userRepo.getUserById(message.getMessageOwner().getId()).getToken());
        full.put("notification", mainObject);
        full.put("data", dataObject);
        fcm.connectorFirebaseServer(full);
        return true;
    }

    public List<Message> getAll() {
        return repo.getAll();
    }

}
