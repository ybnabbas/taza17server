package petauth.kz.message.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import petauth.kz.user.dto.User;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "message")
@Data
public class Message implements Serializable{

    @Id
    @Column(name = "id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "subject")
    private String subject;

    @Column(name = "description")
    private String description;

    @Column(name = "date")
    private Long date=System.currentTimeMillis();

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "messageOwner")
    private User messageOwner;

}
