package petauth.kz.message.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import petauth.kz.message.dto.Message;
import petauth.kz.message.service.MessageService;
import petauth.kz.user.dto.User;
import petauth.kz.user.service.UserService;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    MessageService messageService;

    @Autowired
    UserService userService;

    @GetMapping("/all")
    public List<Message> getAll(){
        return messageService.getAll();
    }



    @GetMapping("/get")
    public List<Message> getOwner() {
        Authentication authentication;
        authentication = SecurityContextHolder.getContext().getAuthentication();

        String username=authentication.getName();
        System.out.println("getCurrentUser is : " +username);
        User owner = userService.getCurrentUser(username);


        return messageService.getAllByOwner(owner);
    }
}
