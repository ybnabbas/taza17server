package petauth.kz.news.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import petauth.kz.news.dto.News;
@Repository
public interface NewsRepo extends JpaRepository<News,Integer> {
}
