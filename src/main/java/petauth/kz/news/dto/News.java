package petauth.kz.news.dto;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "news")
@Data
public class News implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")

    private int id;
    @Column(name = "subject",length = 500)
    private String subject;
    @Column(name = "description",columnDefinition = "TEXT")
    private String description;
    @Column(name = "date")
    private Long date = System.currentTimeMillis();
    @Column(name = "image",columnDefinition = "TEXT")
    private String image;
}
