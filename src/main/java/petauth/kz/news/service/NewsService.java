package petauth.kz.news.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import petauth.kz.news.dao.NewsRepo;
import petauth.kz.news.dto.News;

import java.util.List;

@Service
public class NewsService {
    @Autowired
    NewsRepo repo;


    public List<News> getAll(){
        return repo.findAll();
    }

    public News getById(Integer id){
        return repo.getOne(id);
    }

    public boolean save(News news){

        News result=repo.saveAndFlush(news);
        if (result!=null){
            return true;
        }else {
            return false;
        }
    }
}
