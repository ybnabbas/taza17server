package petauth.kz.news.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import petauth.kz.FCM;
import petauth.kz.news.dto.News;
import petauth.kz.news.service.NewsService;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/news")
public class NewsController {
    @Autowired
    NewsService service;
    @GetMapping("/all")
    public List<News> getAll(){
        return service.getAll();
    }

    @GetMapping
    public News getOne(@RequestParam("id") Integer id){
        return service.getById(id);

    }
    @Autowired
    FCM fcm;
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/save")
    public ResponseEntity save(@RequestBody News news){
// Бұл жерде json  тек мысал ретінде көрсетілген
        if (service.save(news)){
            JSONObject mainObject = new JSONObject();
            mainObject.put("title", news.getSubject());
            mainObject.put("body", news.getDescription());
            mainObject.put("sound", "default");

            JSONObject dataObject = new JSONObject();
            dataObject.put("image",news.getImage());
            dataObject.put("type",1);


            JSONObject full = new JSONObject();
            full.put("to", "/topics/info");
            full.put("notification", mainObject);
            full.put("data", dataObject);
            fcm.connectorFirebaseServer(full);
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.badRequest().build();
        }
    }



}
