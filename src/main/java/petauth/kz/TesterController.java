package petauth.kz;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TesterController {

    @GetMapping("/test")
    public String login(){
        return "test. It is working...";
    }
}
