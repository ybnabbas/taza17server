package petauth.kz.taza.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import petauth.kz.taza.dao.TazaRepo;
import petauth.kz.taza.dto.Statistic;
import petauth.kz.taza.dto.Taza;
import petauth.kz.taza.dto.TazaType;
import petauth.kz.taza.dto.TazaTypeDto;
import petauth.kz.user.dto.User;

import java.util.List;

/**
 * Service class from {@link petauth.kz.taza.dto.Taza}
 */
@Service
public class TazaService {

    @Autowired
    TazaRepo repo;

    public List<Taza> getAllByType(TazaType type) {

        return repo.getAllByType(type);
    }

    public List<Taza> getAllByOwner(User owner) {
        return repo.getAllByOwner(owner);
    }

    public int getStatistic(Long start,Long finish) {

        return repo.getStatistic(start,finish);
    }

    public boolean save(Taza taza) {
        Taza result=repo.saveAndFlush(taza);
        System.out.println("Saved taza object is : " +result);
        return true;
    }

    public List<Taza> getAll() {
        return  repo.getAll();
    }

    public List<TazaType> getTypes(){
        return repo.getTypes();
    }

    public List<Object[]> getAllByGroup(){
        return repo.getAllByGroup();
    }
}
