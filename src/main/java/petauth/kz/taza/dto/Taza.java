package petauth.kz.taza.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import petauth.kz.user.dto.User;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "taza")
@Data
public class Taza implements Serializable {
    @Id
    @Column(name = "id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "type")
    private TazaType type;

    @ManyToOne
    @JoinColumn(name = "tazaOwner")
    @JsonBackReference
    private User tazaOwner;

    @Column(name = "size")
    private int size;

    @Column(name = "date")
    private Long date=System.currentTimeMillis();


}
