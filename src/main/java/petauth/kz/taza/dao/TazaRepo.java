package petauth.kz.taza.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import petauth.kz.taza.dto.Statistic;
import petauth.kz.taza.dto.Taza;
import petauth.kz.taza.dto.TazaType;
import petauth.kz.taza.dto.TazaTypeDto;
import petauth.kz.user.dto.User;

import java.util.List;

@Repository
public interface TazaRepo extends JpaRepository<Taza,Long>{

    @Query("select taza from Taza taza where taza.type=:type")
    List<Taza> getAllByType(@Param("type") TazaType type);

    @Query("select taza from Taza taza where taza.tazaOwner=:owner")
    List<Taza> getAllByOwner(@Param("owner") User owner);

@Query("select count(*) from Taza  taza where taza.date between :start and :finish")
    int getStatistic(@Param("start") Long start,@Param("finish") Long finish);

    @Query(value = "SELECT taza FROM Taza taza ")
    List<Taza> getAll();

    @Query(value = "SELECT tazatype FROM TazaType tazatype ")
    List<TazaType> getTypes();

    @Query(value = "select  tt.name as name,sum(taza.size) as size from Taza taza INNER JOIN taza_type  tt on taza.type=tt.id GROUP BY tt.name",nativeQuery = true)
    List<Object[]> getAllByGroup();
}
