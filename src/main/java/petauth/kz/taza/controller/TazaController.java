package petauth.kz.taza.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import petauth.kz.message.dto.Message;
import petauth.kz.message.service.MessageService;
import petauth.kz.taza.dto.Taza;
import petauth.kz.taza.dto.TazaType;
import petauth.kz.taza.service.TazaService;
import petauth.kz.user.dto.User;
import petauth.kz.user.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/taza")
@CrossOrigin
public class TazaController {

    @Autowired
    TazaService tazaService;
    @Autowired
    UserService userService;

    @Autowired
    MessageService messageService;


    @GetMapping("/type")
    @PreAuthorize("hasAnyRole('USER','MODERATOR','ADMIN')")
    public List<Taza> getAllByType(@RequestParam("type") int id) {
        TazaType type = new TazaType();
        type.setId(id);
        return tazaService.getAllByType(type);
    }

    @GetMapping("/owner")
    public List<Taza> getOwner() {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        String username=authentication.getName();
        System.out.println("getCurrentUser is : " +username);
        User owner = userService.getCurrentUser(username);


        return tazaService.getAllByOwner(owner);
    }
@GetMapping("/all")
    public List<Taza> getAll(){

        return tazaService.getAll();
    }
    @GetMapping("/all/bygroup")
    public List<Object[]> getAllByGroup(){

        return tazaService.getAllByGroup();
    }
    @GetMapping("/types")
    public List<TazaType> getTypes(){

        return tazaService.getTypes();
    }

    @GetMapping("/statistic")
    public int getStatistic(@RequestParam(value = "start") Long start, @RequestParam(value = "finish") Long finish) {
        return tazaService.getStatistic(start, finish);
    }

    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity save(@RequestBody Taza taza) {
        System.out.println("taza is : " + taza);

        if (tazaService.save(taza)) {
            Message message = new Message();
            message.setDate(System.currentTimeMillis());
            message.setMessageOwner(taza.getTazaOwner());
            Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

            String username=authentication.getName();
            message.setSubject(userService.getCurrentUser(username).getFullName());
            message.setDescription("Сізге " +taza.getSize()+ " tazaCoint сәтті түсті");

            messageService.save(message);
            return ResponseEntity.ok(HttpStatus.OK);

        } else {
            return ResponseEntity.ok(HttpStatus.CONFLICT);
        }
    }
}
