package petauth.kz.map.dto;

import petauth.kz.user.dto.User;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "map")
public class Map implements Serializable {
    @Id
    @Column(name = "id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "lat")
    private String lat;
    @Column(name = "lng")
    private String lng;
    @Column(name = "enabled")
    boolean enabled;

    @ManyToOne
    @JoinColumn(name = "mapOwner")
    User mapOwner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public User getMapOwner() {
        return mapOwner;
    }

    public void setMapOwner(User mapOwner) {
        this.mapOwner = mapOwner;
    }
}
