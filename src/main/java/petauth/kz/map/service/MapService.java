package petauth.kz.map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import petauth.kz.map.dao.MapRepo;
import petauth.kz.map.dto.Map;

import java.util.List;

/**
 * Service class for {@link petauth.kz.map.dto.Map}
 *
 * */
@Service
public class MapService {

    @Autowired
    MapRepo repo;

    public List<Map> getAll(){
        return repo.findAll();
    }

    public void save(Map map) {
        Map result=repo.saveAndFlush(map);

        System.out.println("Map saved..." +result.toString());

    }

    public void changeLocale(Map map){
        System.out.println("in service :" +map.toString());
       repo.change(map);
    }
}
