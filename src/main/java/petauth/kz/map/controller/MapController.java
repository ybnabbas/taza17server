package petauth.kz.map.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import petauth.kz.map.dto.Map;
import petauth.kz.map.service.MapService;

import java.util.List;

@RestController
@RequestMapping("/map")
@CrossOrigin
public class MapController {
    @Autowired
    MapService service;

    @GetMapping
    public List<Map> maps(){
        return service.getAll();
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('MODERATOR','ADMIN')")
    public void changeLocale(@RequestBody Map map){
        System.out.println("Get body :" +map.toString() );
         service.changeLocale(map);
    }
}
