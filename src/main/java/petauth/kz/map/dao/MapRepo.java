package petauth.kz.map.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import petauth.kz.map.dto.Map;

import java.util.List;

@Repository
public interface MapRepo extends JpaRepository<Map, Integer> {
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("update Map  map set map.lat=:#{#newData.lat}, map.lng=:#{#newData.lng} where map.id=:#{#newData.id}")
     public void change(@Param("newData")Map newData);


}
